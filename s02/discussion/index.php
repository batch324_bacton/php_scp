<?php require './code.php' ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S02: Selecton Control Structure and Array Manipulation</title>
	</head>
	<body>
		<h1>Repetition Control Structure</h1>

		<h2>While Loop</h2>
		<?php whileLoop(); ?>

		<?php whileLoop(10) ?>
	</body>
</html>