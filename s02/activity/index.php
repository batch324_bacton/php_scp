<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S02 Activity</title>
	</head>
	<body>
		<h2>Activity 1:</h2>

		<h3>Divisible by 5:</h3>
		<?php forLoop(); ?>

		<h2>Activity 2:</h2>

		<?php addStudent("Pedro Penduko"); ?>
		<p><?php var_dump($students); ?></p>
		<p><?php echo count($students); ?></p>

		<?php addStudent("Captain Barbel"); ?>
		<p><?php var_dump($students); ?></p>
		<p><?php echo count($students); ?></p>

		<?php addStudent("Super Ingo"); ?>
		<p><?php var_dump($students); ?></p>
		<p><?php echo count($students); ?></p>

		<?php array_shift($students); ?>
		<p><?php var_dump($students); ?></p>
		<p><?php echo count($students); ?></p>

	</body>
</html>

