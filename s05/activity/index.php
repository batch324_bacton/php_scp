<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login Page</title>
</head>
<body>
    <?php
    session_start();

    // Check if the user is already logged in
    if (isset($_SESSION['username'])) {
        // If logged in, display the email and logout button
        echo "<h1>Welcome!</h1>";
        echo "<p>Hello, {$_SESSION['username']}</p>";
        echo "<form action='logout.php' method='post'>
                <input type='submit' name='logout' value='Logout'>
              </form>";
    } else {
        // If not logged in, show the login form
        echo "<h1>Login</h1>";
        echo "<form action='login.php' method='post'>
                <label for='username'>Username:</label>
                <input type='text' name='username' id='username' required>
                <br>
                <label for='password'>Password:</label>
                <input type='password' name='password' id='password' required>
                <br>
                <input type='submit' value='Login'>
              </form>";

        // Show error message if present
        if (isset($_GET['error']) && $_GET['error'] === '1') {
            echo "<p style='color: red;'>Invalid credentials! Please try again.</p>";
        }
    }
    ?>
</body>
</html>
