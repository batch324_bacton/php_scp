<?php
session_start();

// Check if the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Check if the entered username and password match the correct credentials
    $correctUsername = 'johnsmith@gmail.com';
    $correctPassword = '1234';

    $enteredUsername = $_POST['username'];
    $enteredPassword = $_POST['password'];

    if ($enteredUsername === $correctUsername && $enteredPassword === $correctPassword) {
        // If credentials are correct, set session variables and redirect to index.php
        $_SESSION['username'] = $enteredUsername;
        header('Location: index.php');
    } else {
        // If credentials are incorrect, redirect back to index.php with an error message
        header('Location: index.php?error=1');
    }
} else {
    // If someone accesses login.php directly without submitting the form, redirect to index.php
    header('Location: index.php');
}
