<?php
session_start();

// Unset session variables and destroy the session
$_SESSION = array();
session_destroy();

// Redirect back to index.php
header('Location: index.php');
