<?php require_once "./code.php"?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Activity S01</title>
	</head>

	<body>
		<h1>Full Address:</h1>
		<p><?php echo getFullAddress('Block-12', 'Lot-55', 'Sky Tower', 'Room 35', 'High City', 'Sky haven', 'Encantadia'); ?></p>

		<h1>Letter-Based Grading</h1>
		<p><?php echo getLetterGrade(88); ?></p>
	</body>
</html>