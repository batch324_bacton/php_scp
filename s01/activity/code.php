<?php 

function getFullAddress($block, $lot, $buildingName, $roomName, $city, $province, $country){
	return "$block, $lot, $buildingName, $roomName, $city, $province, $country";
}


function getLetterGrade($grade){
	if ($grade <74){
		return 'D';
	} else if ($grade <=76 && $grade >= 75){
		return 'C-';
	} else if ($grade <=79 && $grade >= 77){
		return 'C';
	} else if ($grade <=82 && $grade >= 80){
		return 'C+';
	} else if ($grade <=85 && $grade >= 83){
		return 'B-';
	} else if ($grade <=88 && $grade >= 86){
		return 'B-';
	} else if ($grade <=91 && $grade >= 89){
		return 'B+';
	} else if ($grade <=94 && $grade >= 92){
		return 'A-';
	} else if ($grade <=97 && $grade >= 95){
		return 'A';
	} else {
		return 'A+';
	}

}


 ?>