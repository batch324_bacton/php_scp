<?php

/*$trial = "Hello World";*/
// [Section] Comments
// Comments are part of the code that gets ignored by the language.
// Comments are meant to describe the algorithm of the written code.

/*
	There are two type of comments:
		- The single-line comment denoted by two forward slashes(//)
		- The multi-line comment denoted by a slash and asterisk (/*)
*/

// Section - Variables
// Variables are used to contain data.
// Variables are named location for the stored value.
// Variables are defined using the dollay ($) notation before the name of the variable.

$name = 'Pedro Penduko';
$email = 'pedropenduko@mail.com';

//[Section] - Constants
//Constants used to hold data that are meant to be read-only.
//Constants are defined using the define() function. 

	/*
		Syntax: 
			define('variableName', valueOfTheVariable);
	*/

define('PI', 3.1416);

//Reassignment of a variable
$name = 'Mang Tomas';

//Section - Data Types in PHP

//Strings

$state = 'New York';
$country = 'United States of America';
$address = $state.', '.$country;

//Reassign the $address
$address = "$state, $country.";

//Integers
$age = 31; 
$headcount = 26;

//floats 
$grade = 98.2;
$distanceInKilometers = 1342.12;

//Boolean
$hasTravelAbroad = false;
$haveSymptoms = true;

//Null
$spouse = null;
$middleName = null;

//Array
$grades = array(98.7, 92.1, 90.2, 94.6);

//Objects
$gradesObj = (object)[
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 94.6
];

$personObj = (object)[
	'fullName' => 'Perdo Penduko',
	'isMarried' => false,
	'age' => 36,
	'address' => (object)[
			'state' => 'New York',
			'country' => 'United States of America'
	],
	'contact' => array('09123456789', '0987654321')
];

//Section - Operators
//Assignment operators

//This operator is used to assign and reassign values of a variable

$x = 1324.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;

//[Section] Functions
//Functions are used to make reusable code.

function getFullName($firsName, $middleInitial, $lastName){
	//you can add here tour algorithm
	return "$lastName, $firsName $middleInitial";
}


//[Section] Selection Control Structures
//Selection control structures are used to make code execution dynamic according to predefined confitions
	//if-elseif-else Statement

function determineTyphoonIntensity($windSpeed){
	//if-elseif-else condition
	if($windSpeed <30){
		return 'Not a Typhoon yet.';
	}else if($windSpeed <= 61){
		return 'Tropical Depression detected.';
	}else if($windSpeed <= 88){
		return 'Tropical Storm detected';
	}else if($windSpeed <= 117){
		return 'Severe Tropical Storm detected';
	}else{
		return 'Typhoon detected';
	}
}

// Conditional (Ternary) Operator

function isUnderAge($age){
	return ($age < 18) ? true : false;
}

// Switch Statement

function determineComputerUser($computerNumber){
	switch($computerNumber) {
		case 1:
			return 'Linus Torvalds';
			break;
		case 2:
			return 'Steve Jobs';
			break;
		case 3:
			return 'Sid Meir';
			break;
		case 4:
			return 'Onel de Guzman';
			break;
		case 5:
			return 'Christian Salvador';
			break;
		default:
			return $computerNumber. ' is out of bounds.';
			break;
	}
}


//Try-Catch-Finally Statement

function greeting($str){
	try{
		//Attempt to execute the code
		if(gettype($str) == "string"){
			//echo $str will run if successful
			echo $str;
		} else {
			//if not, it will return "Oops!"
			throw new Exceptopn("Oops!");
		}
	}
	catch (Exception $e) {
		//Catch the errors within "try"
		echo $e->getMessage();
	}
	finally {
		//Continue execution of code regardless of success and falure of code executiin in 'try' block.
		echo "I did it again!";
	}
}


?>