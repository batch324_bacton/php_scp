<?php 

class Building{

	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	//getter function for name
	public function getName(){
		return $this->name;
	}

	//getter function for floors
	public function getFloors(){
		return $this->floors;
	}

	//getter function for address
	public function getAddress(){
		return $this->address;
	}

	public function printName(){
		return "The name of the building is $this->name.";
	}

	public function checkFloors(){
		return "The $this->name has $this->floors floors.";
	}

}

class Condominium extends Building {

}

$building = new Building('Chaos Tower', 100, 'Sky Heaven, Encantadia');

$condominium = new Condominium('Rapot Condominium', 50, 'Heroes Paradise, Encantadia');


 ?>