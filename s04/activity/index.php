<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Activity 04</title>
	</head>

	<body>
		<h1>Building</h1>
		<p><?php echo $building->printName(); ?></p>

		<p><?php echo $building->checkFloors(); ?></p>

		<h1>Condominium</h1>
		<p><?php echo $condominium->printName(); ?></p>

		<p><?php echo $condominium->checkFloors(); ?></p>
	</body>
</html>